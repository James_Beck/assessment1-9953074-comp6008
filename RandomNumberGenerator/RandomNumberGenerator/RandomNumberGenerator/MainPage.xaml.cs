﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace RandomNumberGenerator
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        public int ReceivedFromXAML;

        void Instructions(object snder, EventArgs e)
        {
            DisplayAlert("Instructions:",
                "Enter in a number from 1 - 9. You're trying to guess what the random number coming up is.",
                "If you perform above 1/10 success over a large sample you have ESP");
        }

        void OnClick (object snder,EventArgs e)
        {
            var a = new RandNum(EntryField.Text);

            var result = a.RandomNumber();

            DarnResult.Text = $"{result}";

            int score;
            string scored = GuessesCorrect.Text;
            score = int.Parse(scored);

            if(EntryField.Text == DarnResult.Text)
            {
                BackgroundColour.BackgroundColor = Color.Green;
                score++;
                GuessesCorrect.Text = $"{score}";
            }
            else
            {
                BackgroundColour.BackgroundColor = Color.Red;
            }            
        }
    }
}
