﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RandomNumberGenerator
{
    class RandNum
    {
        private string Input;
        private Random darn = new Random();

        public RandNum(string _input)
        {
            Input = _input;
        }

        public int RandomNumber()
        {
            var number = 0;
            var output = 0;

            int.TryParse(Input, out output);

            number = darn.Next(1, 10);

            return number;
        }
    }
}
