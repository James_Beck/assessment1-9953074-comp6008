﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Math_Problem
{
    public class Sum3n5
    {
        private string Input;
        
        public Sum3n5(string _input)
        {
            Input = _input;
        }

        public int SumTheMultiples()
        {
            var sum = 0;
            var output = 0;

            bool isNumber = int.TryParse(Input, out output);

            if (isNumber)
            {
                for (int i = 0; i < output; i++)
                {
                    if (i % 3 == 0 || i % 5 == 0)
                    {
                        sum = sum + i;
                    }
                }
            }
            else
            {
               sum = -1337;
            }

            return sum;
        }
    }
}
