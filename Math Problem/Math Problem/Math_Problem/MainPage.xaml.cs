﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Math_Problem
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        public int ReceivedFromXAML;

        void Instructions(object snder, EventArgs e)
        {
            DisplayAlert("Instructions:",
                "This app gets you to input a number. The sum of the multiples of 3 and 5 that belong to your chosen number, minus one, are the response output",
                "Limit yourself to numbers between 1 and 1,000,000");
        }

        void OnClick(object snder, EventArgs e)
        {
            //var input = Class.Method(entryname.Text);
            //LabelName.Text = end variable of Class.Method + input;

            var a = new Sum3n5(EntryField.Text);

            var result = a.SumTheMultiples();

            SumResult.Text = $"{result}";
        }
    }
}
